module.exports = {
    Query: {
        books: (_, __, { dataSources }) =>
            dataSources.booksAPI.getAllBooks(),
        authors: (_, __, { dataSources }) =>
            dataSources.authorsAPI.getAllAuthors()
    }
};
