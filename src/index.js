const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');

const AuthorsAPI = require('./datasources/author_service');
const BooksAPI = require('./datasources/book_service');

const server = new ApolloServer({
    typeDefs,
    resolvers,

    dataSources: () => ({
        authorsAPI: new AuthorsAPI(),
        booksAPI: new BooksAPI()
    })
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
