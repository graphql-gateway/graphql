const { gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    author: String
    numberOfPages: Int
  }
  
  type Author {
    name: String
    age: Int
  }

  type Query {
    books: [Book]
    authors: [Author]
  }
`;

module.exports = typeDefs;
