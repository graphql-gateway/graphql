const { RESTDataSource } = require('apollo-datasource-rest');

class BooksAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'http://192.168.99.100/books-service';
    }

    async getAllBooks() {
        const response = await this.get('books');
        return Array.isArray(response)
            ? response.map(launch => this.reducer(launch))
            : [];
    }

    reducer(launch) {
        return {
            author: launch.author,
            numberOfPages: launch.numberOfPages,
        };
    }
}

module.exports = BooksAPI;
