const { RESTDataSource } = require('apollo-datasource-rest');

class AuthorsAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'http://192.168.99.100/authors-service';
    }

    async getAllAuthors() {
        const response = await this.get('authors');
        return Array.isArray(response)
            ? response.map(launch => this.reducer(launch))
            : [];
    }

    reducer(launch) {
        return {
            name: launch.name,
            age: launch.age,
        };
    }
}

module.exports = AuthorsAPI;
