# GraphQL Gateway

Simple GraphQL Gateway built with Apollo Server fetching data from two different micro-services.

## Getting Started

The project is working with docker-compose so it is necessary to build the Docker images of the different projects.

### Building images

Clone Books-Service and then perform:

```
 docker build -t book_service .
```

Clone Authors-Service and then perform:

```
 docker build -t author_service .
```

Clone GraphQL-Gateway and then perform:

```
 docker build -t graphql .
```

Then perform:

```
 docker-compose up
```